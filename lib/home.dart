import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tugas_elearning/main.dart';
import 'package:tugas_elearning/profile.dart';
import 'function.dart';

class MainScreen extends StatefulWidget {
  final String nama;
  MainScreen({Key key, @required this.nama}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  String namee;

  @override
  void initState() {
    super.initState();
    namee = widget.nama;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Welcome!!!"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Profile"),
                  value: 1,
                )
              ];
            },
            onSelected: (value) {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfileScreen(nama: namee)));
              }
            },
          )
        ],
      ),
      floatingActionButton: customFlatActionButton(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        children: <Widget>[
          Image.asset(
            "assets/images/welcome.webp",
            width: 200.0,
          ),
          Text(
            "Halooo, " + widget.nama + ". Selamat Datang :)",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }

  Widget customFlatActionButton(BuildContext context) {
    return Hero(
      tag: "Profile",
      child: SizedBox(
        height: 40.0,
        width: 90.0,
        child: RawMaterialButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0)),
          elevation: 10.0,
          fillColor: Theme.of(context).accentColor,
          child: Center(child: Text("Profile")),
          onPressed: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProfileScreen(nama: namee)));
          },
        ),
      ),
    );
  }
}
