class Validation {
  String validateName(String value) {
    if (value.isEmpty) {
      //JIKA VALUE KOSONG
      return 'Nama Harus Diisi!'; //MAKA PESAN DITAMPILKAN
    } else if (value != "Yusuf" ||
        value != "Marisa" ||
        value != "Ivan Syah" ||
        value != "Huda" ||
        value != "Destriyana") {
      return 'Nama tidak sesuai!';
    } else {
      return null;
    }
  }
}
