import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyFunction {
  static void focusSwitcher(
      BuildContext context, FocusNode curFocus, FocusNode nextFocust) {
    curFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocust);
  }
}
